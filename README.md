
# Shintaku

An end-to-end-decentralized general-purpose blockchain oracle system.

## Testing

Install dependencies:

```bash
npm install
export PATH="./node_modules/.bin:${PATH}"
```

Test using truffle:

```bash
truffle test
```

## Token emission schedule

The Shintaku token uses a linear decay with tail emission schedule.
Each "period," the current maximum number of tokens to be minted decreases by 4% of the initial number, until tail emission of a flat 4% of the initial (e.g. 100%, 96%, ..., 8%, 4%) after 25 periods.
Each period lasts 100,000 blocks, or approximately 2.5 weeks.

Each period, a maximum number of tokens to mint is calculated by the contract, along with a fixed maximum amount of ether to accept.
If purchases in the period do not exceed this maximum, tokens are distributed at the calculated strike price.
If purchases in the period exceed this maximum, tokens are allocated proportionally to each user's contribution with respect to the amount of ether collected.
Remaining ether is locked for a long period of time (25 periods, or ~1 year), after which it can be withdrawn.

The token contract owner may withdraw collected ether after a long period of time (50 periods, or ~2 years).

## Interacting with the contract

Shintaku is split into two contracts, the token contract (that solely manages token transfer and emission logic) and the oracle contract (that solely manages the oracle core logic).
Tokens are deposited into the oracle contract for use, and withdrawn when no longer needed.

## Purchasing tokens

In order to participate in the system, tokens must be used.
They may be purchased on the open market, or directly from the contract.
Purchasing of tokens from the contract requires no intermediaries or whitelisting: the system is end-to-end decentralized.
Two methods can be used to purchase tokens, depending on how much effort the user wants to go through to prevent their purchase from being potentially  gamed.
**Note: read the above section on the emission schedule carefully before sending any ether to this contract.**

### The easy way

Call the function `placeOpenPurchaseOrder(address _alias)`, sending however much ether you want to use to purchase tokens.
The alias is the address used to eventually claim tokens for (e.g. your cold storage wallet address).

When purchasing, pay careful attention to the current period, which can be queried with `currentPeriodIndex()`.

Tokens can be claimed with the `claim(address _from, uint _period)` function after two periods have passed.
Pass in the address you used to make the purchase (not the alias) and the period in which you placed your purchase.

### The not-so-easy way

This method uses a similar commit/reveal scheme to ENS.
Begin by crafting a sealed purchase with `createPurchaseOrder(address _from, uint _period, uint _value, bytes32 _salt)`, with the value in wei you eventually want to use to purcahse tokens with, and a random salt (generate a random number locally).

Then call the function `placePurchaseOrder(bytes32 _sealedPurchaseOrder)`
, sending however much ether you want, as long as this is greater or equal to the `_value` parameter in the purchase order.

When purchasing, pay careful attention to the current period, which can be queried with `currentPeriodIndex()`.

In the subsequent period, and no later, your sealed purchase order must be revealed with `revealPurchaseOrder(bytes32 _sealedPurchaseOrder, uint _period, uint _value, bytes32 _salt, address _alias)`.
The alias is the address used to eventually claim tokens for (e.g. your cold storage wallet address).
Excess ether is automagically sent to the alias address.

Finally, tokens can be claimed with the `claim(address _from, uint _period)` function after two periods have passed.
Pass in the address you used to make the purchase (not the alias) and the period in which you placed your purchase.
