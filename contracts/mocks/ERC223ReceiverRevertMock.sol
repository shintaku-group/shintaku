pragma solidity ^0.4.24;

import '../libs/ERC223Receiver_Interface.sol';

/**
 * @title Mock implementation of ERC223Receiver interface. This always reverts.
 */
contract ERC223ReceiverRevertMock is ERC223Receiver {
    event TokenFallback(address _from, uint _value, bytes _data);

    function tokenFallback(address _from, uint _value, bytes _data) public {
        emit TokenFallback(_from, _value, _data);
        revert();
    }
}
