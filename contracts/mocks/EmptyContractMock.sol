pragma solidity ^0.4.24;

/**
 * @title Empty contract; doesn't implement ERC223 receiver interface.
 */
contract EmptyContractMock {
}
