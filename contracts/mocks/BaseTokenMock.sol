pragma solidity ^0.4.24;

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

import '../libs/BaseToken.sol';

/**
 * @title Mock implementation of base ERC223Token that can modify balances at will.
 */
contract BaseTokenMock is BaseToken {
    using SafeMath for uint;

    function debugAddBalance(address _account, uint _value) public {
        balances[_account] = balances[_account].add(_value);
        totalSupply_ = totalSupply_.add(_value);
    }

    function debugRemovebalance(address _account, uint _value) public {
        balances[_account] = balances[_account].sub(_value);
        totalSupply_ = totalSupply_.sub(_value);
    }
}

