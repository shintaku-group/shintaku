pragma solidity ^0.4.24;

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

import './OracleInterface.sol';

/**
 * @title Shintaku oracle platform contract
 * @dev Uses stakes from token contract for voting-based oracle.
 */
contract Shintaku is Ownable, OracleInterface {
    using SafeMath for uint;

    // Custom types

    // Define stages 
    //  AcceptPropositions: Until there are a minimum number of propositions in list, accept propositions.
    //   Note: propositions can be accepted at any stage.
    //  CommitRandom: Users who want to vote commit a bond and a sealed locally-generated random number.
    //  RevealRandom: Users who committed a sealed random number in the previous stage may reveal.
    //  CommitVote: Users who revealed their random numbers in the previous stage may cast sealed votes.
    //  RevealVote: Users who committed sealed votes in the previous stage may reveal their votes.
    enum Stage { AcceptPropositions, CommitRandom, RevealRandom, CommitVote, RevealVote }
    // Permissions for manipulating the RanDAO
    enum RanDAOPermission { None, RevealVote, SubmitVote, RevealRandomNumber }

    // Pair of votes
    struct VotePair {
        bytes32[] propIDs;
        VoteOption[] options;
        uint stake;
    }

    // Stores data for proposition
    struct Proposition {
        // Unique proposition ID
        bytes32 id;

        // Bounty attached to proposition
        uint bounty;
        // Decided outcome
        VoteOption decision;

        // Total voting stake for each option
        mapping (uint => uint) voteStakesTotals;
        // Total voting stake
        uint voteStakesTotal;
    }

    struct Round {
        // Rounds are uniquely identified by the start index of undecided propositions.
        uint id;

        // RanDAO permissions for each account
        mapping (address => mapping(bytes32 => RanDAOPermission)) ranDAOPermissions;

        // Number of unsealed random numbers (sealed - revealed)
        uint unsealedRandomNumbersCount;
        // Sealed votes for each account
        mapping (address => mapping (bytes32 => bool)) sealedVotes;
        // Number of revealed votes
        uint revealedVotesCount;
        // Total bounty for propositions in this round
        uint bountyTotal;

        // Vote pairs for each voter
        mapping (address => VotePair[]) votePairs;
    }

    struct Clique {
        // Unique clique ID: the clique creator
        address id;
        // A creator-provided description of the clique
        string description;

        // Parameters

        // If this is true, only the clique creator can submit propositions
        bool permissioned;

        // If set, generates random numbers securely, but is more expensive and slower
        bool secureRNG;

        // Minimum number of propositions to vote on simultaneously
        uint minPropositionListSize;
        // Minimum bounty to be attached to a proposition
        uint minBounty;
        // Maximum bounty that can be attached to a proposition: 0 for any
        uint maxBounty;
        // The stake of a single vote
        uint voteStake;
        // Duration of stage in blocks
        uint stageDuration;
        // Ratio for determining majority
        // For simple majority, set mul:1 div:2
        uint majorityMul;
        uint majorityDiv;
        // Factor of safety for random number generation
        //  Voters are required to post a bond of this many voting stakes when
        //  submitting a sealed random number.
        uint randomFactorOfSafety;

        // State

        // State of each proposition
        mapping (bytes32 => Proposition) propositions;

        // Array of all submitted propositions
        bytes32[] propositionList;
        // Start index of undecided propositions. Uniquely defines rounds.
        uint propositionListStart;
        // Count of propositions being voted on
        uint propositionListCount;

        // Holds current stage
        Stage stage;
        // Block at which current stage started
        uint stageStartBlock;
        // Accumulated RANDOM random number
        bytes32 randomNumber;

        // Voting rounds
        mapping (uint => Round) rounds;

        // For permissioned cliques, whitelist of accounts that can vote
        mapping (address => bool) whitelist;
    }

    // Modifiers

    modifier onlyInStage(address _clique, Stage _stage) {
        require(cliques[_clique].stage == _stage);
        _;
    }

    modifier cliqueExists(address _clique) {
        require(cliques[_clique].id != 0);
        _;
    }

    // Contract state

    // Proposition cliques
    mapping (address => Clique) internal cliques;

    // Function definitions

    /**
     * @dev Constructor.
     * @param _tokenAddress Address of the token contract to use.
     */
    constructor(
        address _tokenAddress
    )
        Forkable(_tokenAddress)
        public
    {
    }

    // Oracle interface definitions

    /**
     * @dev Checks if a proposition is decided. Reverts if proposition is invalid.
     * @param _clique The ID of the clique.
     * @param _propID The proposition ID.
     * @return Whether the proposition is decided or not.
     */
    function isPropositionDecided(address _clique, bytes32 _propID) public view cliqueExists(_clique) returns (bool) {
        Clique storage clique = getCliqueP(_clique);
        Proposition storage proposition = getPropositionP(clique, _propID);
        require(proposition.id != 0);

        return (proposition.decision != VoteOption.Undecided);
    }

    /**
     * @dev Get the outcome of a decided proposition. Reverts if proposition is undecided or otherwise invalid.
     * @param _clique The ID of the clique.
     * @param _propID The proposition ID.
     * @return The outcome of the voting.
     */
    function getPropositionResult(address _clique, bytes32 _propID) public view cliqueExists(_clique) returns (VoteOption) {
        Clique storage clique = getCliqueP(_clique);
        Proposition storage proposition = getPropositionP(clique, _propID);
        require(proposition.id != 0);
        require(proposition.decision != VoteOption.Undecided);

        return proposition.decision;
    }

    // General contract management functions

    /**
     * @dev Create a new proposition clique. Cliques are uniquely identified by
     *          the creator's address. Note that cliques cannot be transferred
     *          to another owner once created. Parameters should be carefully
     *          selected by the clique creator.
     * @param _description A string description of the clique, possibly with
     *          how to interpret the proposition ID bytes.
     * @param _permissioned Boolean indicating if the clique is permissioned or
     *          not. Permissioned cliques may only be voted on by accounts
     *          whitelisted within that clique, by the clique creator.
     * @param _secureRNG Boolean indicating if the RANDAO should use safe random
     *          number generation or not. If true, the random number is not
     *          manipulable, but there are no termination guarantees. If false,
     *          the random number _may_ be manipulated with non-trivial effort,
     *          but the process is guaranteed to terminate. In practice, there
     *          is little reason to use the secure RNG method.
     * @param _minPropositionListSize The minimum size of the proposition list
     *          needed to start voting. This should be set relatively large (at
     *          least 10) in order to make manipulation of a single proposition
     *          more expensive.
     * @param _minBounty The minimum bounty that needs to be attached to
     *          propositions by submitters.
     * @param _maxBounty The maximum bounty that can be attached to propositions
     *          by submitters. Optional: if set to 0, then there is no maximum.
     * @param _voteStake The stake required for a single vote (vote pairs
     *          require two times this amount).
     * @param _stageDuration The duration of each stage, in blocks.
     * @param _majorityMul The voting stake required for agreement is
     *          multiplied by this. Set to 1 for simple majority.
     * @param _majorityDiv The voting stake required for agreement is divided by
     *          this. Set to 2 for simple majority.
     * @param _randomFactorOfSafety A factor of safety for random number
     *          generation bonds. Voters who submit a sealed random number must
     *          lock up this times factor multiplied by twice the voting stake.
     *          This factor of safety is returned upon random number reveal.
     */
    function newClique(
        string _description,
        bool _permissioned,
        bool _secureRNG,
        uint _minPropositionListSize,
        uint _minBounty,
        uint _maxBounty,
        uint _voteStake,
        uint _stageDuration,
        uint _majorityMul,
        uint _majorityDiv,
        uint _randomFactorOfSafety
    )
        public
    {
        require(cliques[msg.sender].id == 0);
        assert(msg.sender != address(0));
        // Since we're doing pair voting, list size must be at least 2
        require(_minPropositionListSize >= 2);
        require(_minBounty > 0);
        require(_maxBounty == 0 || _maxBounty > _minBounty);
        require(_voteStake > 0);
        require(_stageDuration >= 1);
        require(_majorityMul > 0);
        require(_majorityMul < _majorityDiv);
        require(_randomFactorOfSafety >= 1);

        cliques[msg.sender] = Clique({
            id: msg.sender,
            description: _description,
            permissioned: _permissioned,
            secureRNG: _secureRNG,
            minPropositionListSize: _minPropositionListSize,
            minBounty: _minBounty,
            maxBounty: _maxBounty,
            voteStake: _voteStake,
            stageDuration: _stageDuration,
            majorityMul: _majorityMul,
            majorityDiv: _majorityDiv,
            randomFactorOfSafety: _randomFactorOfSafety,
            propositionList: new bytes32[](0),
            propositionListStart: 0,
            propositionListCount: 0,
            stage: Stage.AcceptPropositions,
            stageStartBlock: block.number,
            randomNumber: bytes32(0)
        });

        emit CliqueNew(msg.sender);
    }

    /**
     * @dev For permissioned cliques, add account to whitelist.
     * @param _clique The ID of the clique.
     * @param _account The account to add to whitelist.
     */
    function cliqueWhitelistAdd(address _clique, address _account) public cliqueExists(_clique) {
        Clique storage clique = getCliqueP(_clique);
        require(msg.sender == clique.id);
        require(clique.permissioned == true);

        clique.whitelist[_account] = true;

        emit CliqueWhitelistAdd(_clique, _account);
    }

    /**
     * @dev For permissioned cliques, remove account from whitelist.
     * @param _clique The ID of the clique.
     * @param _account The account to remove from whitelist.
     */
    function cliqueWhitelistRemove(address _clique, address _account) public cliqueExists(_clique) {
        Clique storage clique = getCliqueP(_clique);
        require(msg.sender == clique.id);
        require(clique.permissioned == true);

        delete clique.whitelist[_account];

        emit CliqueWhitelistRemove(_clique, _account);
    }

    /**
     * @dev Move a clique to the next stage.
     * @param _clique The ID of the clique.
     * @param _count For accept stage only, the number of propositions to start vote on (0 for max).
     */
    function nextStage(address _clique, uint _count) public cliqueExists(_clique) {
        Clique storage clique = getCliqueP(_clique);
        Stage stage = clique.stage;
        assert(
            stage == Stage.AcceptPropositions ||
            stage == Stage.CommitRandom ||
            stage == Stage.RevealRandom ||
            stage == Stage.CommitVote ||
            stage == Stage.RevealVote
        );

        uint i;
        bytes32 propID;
        uint propositionListStart = clique.propositionListStart;

        if (stage == Stage.AcceptPropositions) {
            uint count = clique.propositionList.length - propositionListStart;
            if (_count > 0) {
                count = _count;
            }
            require(count >= clique.minPropositionListSize);
            clique.propositionListCount = count;

            uint bountyTotal = 0;

            for (i = propositionListStart; i < propositionListStart + count; i++) {
                propID = clique.propositionList[i];
                bountyTotal = bountyTotal.add(getPropositionP(clique, propID).bounty);
            }

            clique.stage = Stage.CommitRandom;
            clique.stageStartBlock = block.number;

            clique.rounds[propositionListStart] = Round({
                id: propositionListStart,
                unsealedRandomNumbersCount: 0,
                revealedVotesCount: 0,
                bountyTotal: bountyTotal
            });

            emit CliqueStage(_clique, uint(clique.stage), propositionListStart);

            return;
        }

        require(block.number.sub(clique.stageStartBlock) > clique.stageDuration);

        Round storage round = clique.rounds[propositionListStart];

        if (stage == Stage.CommitRandom) {
            clique.stage = Stage.RevealRandom;
        } else if (stage == Stage.RevealRandom) {
            if (clique.secureRNG && round.unsealedRandomNumbersCount != 0) {
                // If using secure RNG, check that number of sealed random
                //  numbers equals number of revealed random numbers
                delete round.unsealedRandomNumbersCount;
                clique.stage = Stage.AcceptPropositions;
                clique.stageStartBlock = block.number;

                emit CliqueStage(_clique, uint(clique.stage), propositionListStart);

                return;
            }
            delete round.unsealedRandomNumbersCount;
            clique.stage = Stage.CommitVote;
        } else if (stage == Stage.CommitVote) {
            clique.stage = Stage.RevealVote;
        } else if (stage == Stage.RevealVote) {
            clique.stage = Stage.AcceptPropositions;
            uint propositionListEnd = propositionListStart.add(clique.propositionListCount);
            for (i = propositionListStart; i < propositionListEnd; i++) {
                propID = clique.propositionList[i];
                Proposition storage proposition = getPropositionP(clique, propID);
                // A configurable majority is needed to be "correct"
                uint minimumMajorityStake = proposition.voteStakesTotal.mul(clique.majorityMul).div(clique.majorityDiv);

                if (proposition.voteStakesTotals[uint(VoteOption.True)] > minimumMajorityStake) {
                    // If majority TRUE
                    proposition.decision = VoteOption.True;
                } else if (proposition.voteStakesTotals[uint(VoteOption.False)] > minimumMajorityStake) {
                    // If majority FALSE
                    proposition.decision = VoteOption.False;
                } else {
                    // If not majority, then unknown
                    proposition.decision = VoteOption.Unknown;
                }

                emit CliquePropositionDecided(_clique, proposition.id, proposition.decision);

                delete clique.propositionList[i];
            }

            // Offset the start of undecided propositions
            clique.propositionListStart = propositionListEnd;

            delete clique.randomNumber;
        } else {
            // This should never happen
            revert();
        }

        clique.stageStartBlock = block.number;

        emit CliqueStage(_clique, uint(clique.stage), clique.propositionListStart);
    }

    // Submitter functions

    /**
     * @dev Submit a proposition.
     * @param _clique The ID of the clique.
     * @param _propID The proposition ID. This can interpreted in any way, by
     *          each clique individually. It is recommended to use a hash of the
     *          message and distribute it via a decentralized storage system
     *          e.g. IPFS. The message could be e.g. a JSON object following a
     *          community-established standard.
     * @param _bounty The bounty, in tokens.
     */
    function submitProposition(address _clique, bytes32 _propID, uint _bounty) public cliqueExists(_clique) {
        Clique storage clique = getCliqueP(_clique);
        require(_propID != bytes32(0));
        require(_bounty > 0);
        require(_bounty >= clique.minBounty);
        require(clique.maxBounty == 0 || _bounty <= clique.maxBounty);
        require(balances[msg.sender] >= _bounty);
        // No conflicts in proposition ID
        require(clique.propositions[_propID].id == 0);

        if (clique.permissioned) {
            require(msg.sender == clique.id);
        }

        balances[msg.sender] = balances[msg.sender].sub(_bounty);
        clique.propositions[_propID] = Proposition(_propID, _bounty, VoteOption.Undecided, 0);
        clique.propositionList.push(_propID);

        emit CliqueSubmitProposition(_clique, msg.sender, _propID, _bounty);
    }

    /**
     * @dev Submit multiple propositions.
     * @param _clique The ID of the clique.
     * @param _propIDs The proposition IDs.
     * @param _bounties The bounties, in tokens.
     */
    function submitPropositions(address _clique, bytes32[] _propIDs, uint[] _bounties) public {
        require(_propIDs.length == _bounties.length);

        for (uint i = 0; i < _propIDs.length; i++) {
            submitProposition(_clique, _propIDs[i], _bounties[i]);
        }
    }

    // Voter functions

    /**
     * @dev Create a sealed random number. Access to the RanDAO is uniquely identified by this.
     * @param _clique The ID of the clique.
     * @param _voter Address of the voter.
     * @param _N The random number.
     * @return The sealed random number.
     */
    function createSealedRandomNumber(address _clique, address _voter, uint _N) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(_clique, _voter, _N));
    }

    /**
     * @dev Submit a sealed random number. The voting stake is locked at this time in addition to a bond.
     * @param _clique The ID of the clique.
     * @param _sealedRandomNumber The sealed random number.
     */
    function submitSealedRandomNumber(
        address _clique,
        bytes32 _sealedRandomNumber
    )
        public
        cliqueExists(_clique)
        onlyInStage(_clique, Stage.CommitRandom)
    {
        Clique storage clique = getCliqueP(_clique);
        uint bond = clique.voteStake.mul(2).mul(clique.randomFactorOfSafety);
        require(balances[msg.sender] >= bond);
        Round storage round = getCurrentRoundP(clique);
        require(round.ranDAOPermissions[msg.sender][_sealedRandomNumber] == RanDAOPermission.None);

        if(clique.permissioned) {
            require(clique.whitelist[msg.sender] == true);
        }

        round.unsealedRandomNumbersCount = round.unsealedRandomNumbersCount.add(1);
        round.ranDAOPermissions[msg.sender][_sealedRandomNumber] = RanDAOPermission.RevealRandomNumber;
        balances[msg.sender] = balances[msg.sender].sub(bond);
    }

    /**
     * @dev Submit multiple sealed random number. The voting stake is locked at this time in addition to a bond.
     * @param _clique The ID of the clique.
     * @param _sealedRandomNumbers The sealed random number.
     */
    function submitSealedRandomNumbers(
        address _clique,
        bytes32[] _sealedRandomNumbers
    )
        public
        cliqueExists(_clique)
        onlyInStage(_clique, Stage.CommitRandom)
    {
        for (uint i = 0; i < _sealedRandomNumbers.length; i++) {
            submitSealedRandomNumber(_clique, _sealedRandomNumbers[i]);
        }
    }

    /**
     * @dev Reveal a sealed random number.
     * @param _clique The ID of the clique.
     * @param _sealedRandomNumber The sealed random number.
     * @param _N The random number.
     */
    function revealSealedRandomNumber(
        address _clique,
        bytes32 _sealedRandomNumber,
        uint _N
    )
        public
        cliqueExists(_clique)
        onlyInStage(_clique, Stage.RevealRandom)
    {
        Clique storage clique = getCliqueP(_clique);
        Round storage round = getCurrentRoundP(clique);
        require(round.ranDAOPermissions[msg.sender][_sealedRandomNumber] == RanDAOPermission.RevealRandomNumber);

        require(_sealedRandomNumber == createSealedRandomNumber(_clique, msg.sender, _N));

        round.unsealedRandomNumbersCount = round.unsealedRandomNumbersCount.sub(1);
        round.ranDAOPermissions[msg.sender][_sealedRandomNumber] = RanDAOPermission.SubmitVote;
        clique.randomNumber = keccak256(abi.encodePacked(clique.randomNumber, _N));

        // Refund factor of safety on successful reveal
        balances[msg.sender] = balances[msg.sender].add(clique.voteStake.mul(2).mul(clique.randomFactorOfSafety.sub(1)));

        emit CliqueRandomNumberReveal(_clique, clique.propositionListStart, msg.sender, _N);
    }

    /**
     * @dev Create a sealed vote to be submitted.
     * @param _clique The ID of the clique.
     * @param _voter Address of the voter.
     * @param _propIDs The proposition IDs.
     * @param _options The vote options.
     * @param _salt A random value to make sure vote is secret.
     * @return The sealed vote.
     */
    function createSealedVote(
        address _clique,
        address _voter,
        bytes32[] _propIDs,
        VoteOption[] _options,
        bytes32 _salt
    )
        public
        pure
        returns (bytes32)
    {
        require(_propIDs.length == 2 && _options.length == 2);
        return keccak256(abi.encodePacked(_clique, _voter, _propIDs, _options, _salt));
    }

    /**
     * @dev Submit a previously-created sealed vote.
     * @param _clique The ID of the clique.
     * @param _sealedVote The sealed vote.
     * @param _sealedRandomNumber The sealed random number.
     */
    function submitSealedVote(
        address _clique,
        bytes32 _sealedVote,
        bytes32 _sealedRandomNumber
    )
        public
        cliqueExists(_clique)
        onlyInStage(_clique, Stage.CommitVote)
    {
        Clique storage clique = getCliqueP(_clique);
        Round storage round = getCurrentRoundP(clique);
        require(round.ranDAOPermissions[msg.sender][_sealedRandomNumber] == RanDAOPermission.SubmitVote);

        round.ranDAOPermissions[msg.sender][_sealedRandomNumber] = RanDAOPermission.RevealVote;
        round.sealedVotes[msg.sender][_sealedVote] = true;
    }

    /**
     * @dev Reveal a vote during the reveal stage.
     * @param _clique The ID of the clique.
     * @param _sealedVote The sealed vote.
     * @param _propIDs The proposition IDs.
     * @param _options The vote options.
     * @param _salt A random value to make sure vote is secret.
     * @param _sealedRandomNumber The sealed random number.
     */
    function revealSealedVote(
        address _clique,
        bytes32 _sealedVote,
        bytes32[] _propIDs,
        VoteOption[] _options,
        bytes32 _salt,
        bytes32 _sealedRandomNumber
    )
        public
        cliqueExists(_clique)
        onlyInStage(_clique, Stage.RevealVote)
    {
        Clique storage clique = getCliqueP(_clique);
        require(_propIDs.length == 2 && _options.length == 2);
        for (uint i = 0; i < 2; i++) {
            require(_options[i] == VoteOption.True || _options[i] == VoteOption.False || _options[i] == VoteOption.Unknown);
            Proposition storage proposition = getPropositionP(clique, _propIDs[i]);
            require(proposition.decision == VoteOption.Undecided);
            proposition.voteStakesTotals[uint(_options[i])] = proposition.voteStakesTotals[uint(_options[i])].add(clique.voteStake);
            proposition.voteStakesTotal = proposition.voteStakesTotal.add(clique.voteStake);
        }
        Round storage round = getCurrentRoundP(clique);
        require(round.ranDAOPermissions[msg.sender][_sealedRandomNumber] == RanDAOPermission.RevealVote);
        require(round.sealedVotes[msg.sender][_sealedVote] == true);

        // If sealed votes don't match, revert (don't return stake if no matching vote provided)
        require(_sealedVote == createSealedVote(_clique, msg.sender, _propIDs, _options, _salt));

        checkPropositionListOffsets(_clique, clique.randomNumber, _sealedRandomNumber, clique.propositionListCount, _propIDs);

        round.ranDAOPermissions[msg.sender][_sealedRandomNumber] = RanDAOPermission.None;

        round.sealedVotes[msg.sender][_sealedVote] = false;
        round.revealedVotesCount = round.revealedVotesCount.add(1);

        round.votePairs[msg.sender].push(VotePair(_propIDs, _options, clique.voteStake));

        emit CliqueRevealVote(_clique, clique.propositionListStart, msg.sender, _propIDs, _options);
    }

    /**
     * @dev Collects reward for a vote.
     * @param _clique The ID of the clique.
     * @param _voter Address of the voter.
     * @param _start The start index of the round.
     */
    function collectVoterReward(address _clique, address _voter, uint _start) public cliqueExists(_clique) {
        Clique storage clique = getCliqueP(_clique);
        Round storage round = clique.rounds[_start];
        VotePair[] storage votePairs = round.votePairs[msg.sender];
        require(votePairs.length > 0);

        // The number of tokens to return to the voter
        uint reward = 0;

        // TODO maybe refactor this function to only collect reward for last element of array?
        for (uint i = 0; i < votePairs.length; i++) {
            VotePair storage votePair = votePairs[i];
            for (uint j = 0; j < 2; j++) {
                Proposition storage proposition = getPropositionP(clique, votePair.propIDs[i]);
                require(proposition.decision != VoteOption.Undecided);
                // For correct vote, return stake
                if (proposition.decision == votePair.options[i]) {
                    reward = reward.add(votePair.stake);
                }
            }

            // For different votes in the pair, also reward
            // Reward is proportional total bounty of this round divided by number of revealed votes
            if (votePair.options[0] != votePair.options[1]) {
                reward = reward.add(round.bountyTotal.div(round.revealedVotesCount));
            }
        }
        delete round.votePairs[msg.sender];

        // Return stakes factoring in rewards and penalties
        balances[_voter] = balances[_voter].add(reward);
    }

    // Internal functions

    /**
     * @dev Get storage pointer to clique at given ID.
     * @param _clique The ID of the clique.
     * @return Storage pointer to clique.
     */
    function getCliqueP(address _clique) internal view returns (Clique storage) {
        return cliques[_clique];
    }

    /**
     * @dev Get storage pointer for proposition in clique.
     * @param _clique Storage pointer of clique.
     * @param _propID The proposition ID.
     * @return Storage pointer to proposition.
     */
    function getPropositionP(Clique storage _clique, bytes32 _propID) internal view returns (Proposition storage) {
        return _clique.propositions[_propID];
    }

    /**
     * @dev Get storage pointer to clique's current round.
     * @param _clique Storage pointer of clique.
     * @return Storage pointer to round.
     */
    function getCurrentRoundP(Clique storage _clique) internal view returns (Round storage) {
        return _clique.rounds[_clique.propositionListStart];
    }

    /**
     * @dev Helper function to get verifiable proposition offset.
     * @param _clique The ID of the clique.
     * @param _number The random accumulated number.
     * @param _sealedNumber The sealed random number.
     * @param _count Proposition list size.
     * @param _propIDs The proposition IDs for the first proposition.
     */
    function checkPropositionListOffsets(
        address _clique,
        bytes32 _number,
        bytes32 _sealedNumber,
        uint _count,
        bytes32[] _propIDs
    )
        internal
        view
        cliqueExists(_clique)
    {
        Clique storage clique = getCliqueP(_clique);
        assert(_propIDs.length == 2);

        uint[] memory propositionListOffsets = getPropositionListOffsets(_number, _sealedNumber, _count);
        assert(propositionListOffsets.length == 2);

        // Check that this voter had permission to vote
        for (uint i = 0; i < 2; i++) {
            require(clique.propositionList[clique.propositionListStart.add(propositionListOffsets[i])] == _propIDs[i]);
        }
    }

    /*
     * @dev Helper function to get offsets in proposition list from random number.
     * @param _number Accumulated random number in RanDAO.
     * @param _sealedNumber The sealed random number.
     * @param _count Size of proposition list.
     * @return Array of two offsets.
     */
    function getPropositionListOffsets(bytes32 _number, bytes32 _sealedNumber, uint _count) public pure returns (uint[]) {
        uint[] memory propositionListOffsets = new uint[](2);

        // The random number hashed with the sealed random number from the user,
        // modulus the proposition list size gives the index of the first proposition
        bytes32 h1 = keccak256(abi.encodePacked(_number, _sealedNumber));
        propositionListOffsets[0] = uint(h1) % _count;

        // The second proposition takes the hash of the above hash, and mods it
        // with the size of the list minus 1 (no collisions) to get a random offset
        // over the remaining available offsets
        bytes32 h2 = keccak256(abi.encodePacked(h1));
        propositionListOffsets[1] = uint(h2) % (_count - 1);
        if (propositionListOffsets[1] >= propositionListOffsets[0]) {
            propositionListOffsets[1] = propositionListOffsets[1] + 1;
        }

        return propositionListOffsets;
    }

    // Public getters

    // Clique getters

    function doesCliqueExist(address _clique) public view returns (bool) {
        return (cliques[_clique].id != address(0));
    }

    function getCliqueDescription(address _clique) public view returns (string) {
        return cliques[_clique].description;
    }

    function isCliquePermissioned(address _clique) public view returns (bool) {
        return cliques[_clique].permissioned;
    }

    function isCliqueRNGSecure(address _clique) public view returns (bool) {
        return cliques[_clique].secureRNG;
    }

    function getCliqueMinPropositionListSize(address _clique) public view returns (uint) {
        return cliques[_clique].minPropositionListSize;
    }

    function getCliqueMinBounty(address _clique) public view returns (uint) {
        return cliques[_clique].minBounty;
    }

    function getCliqueMaxBounty(address _clique) public view returns (uint) {
        return cliques[_clique].maxBounty;
    }

    function getCliqueVoteStake(address _clique) public view returns (uint) {
        return cliques[_clique].voteStake;
    }

    function getCliqueStageDuration(address _clique) public view returns (uint) {
        return cliques[_clique].stageDuration;
    }

    function getCliqueMajority(address _clique) public view returns (uint, uint) {
        return (cliques[_clique].majorityMul, cliques[_clique].majorityDiv);
    }

    function getCliqueRandomFactorOfSafety(address _clique) public view returns (uint) {
        return cliques[_clique].randomFactorOfSafety;
    }

    function isWhitelistedForClique(address _clique, address _account) public view returns (bool) {
        return cliques[_clique].whitelist[_account];
    }

    // Clique proposition getters

    function doesCliquePropositionExist(address _clique, bytes32 _propID) public view returns (bool) {
        return (cliques[_clique].propositions[_propID].id != bytes32(0));
    }

    function getCliquePropositionBounty(address _clique, bytes32 _propID) public view returns (uint) {
        return cliques[_clique].propositions[_propID].bounty;
    }

    function getCliquePropositionDecision(address _clique, bytes32 _propID) public view returns (uint) {
        return uint(cliques[_clique].propositions[_propID].decision);
    }

    function getCliquePropositionVoteStakesTotalFor(address _clique, bytes32 _propID, uint _option) public view returns (uint) {
        return cliques[_clique].propositions[_propID].voteStakesTotals[_option];
    }

    function getCliquePropositionVoteStakesTotal(address _clique, bytes32 _propID) public view returns (uint) {
        return cliques[_clique].propositions[_propID].voteStakesTotal;
    }

    // Clique round getters

    function doesCliqueRoundExist(address _clique, uint _round) public view returns (bool) {
        return (cliques[_clique].rounds[_round].id != uint(0));
    }

    function getCliqueRoundUnsealedRandomNumbersCount(address _clique, uint _round) public view returns (uint) {
        return cliques[_clique].rounds[_round].unsealedRandomNumbersCount;
    }

    function getCliqueRoundRevealedVotesCount(address _clique, uint _round) public view returns (uint) {
        return cliques[_clique].rounds[_round].revealedVotesCount;
    }

    function getCliqueRoundBountyTotal(address _clique, uint _round) public view returns (uint) {
        return cliques[_clique].rounds[_round].bountyTotal;
    }
}

