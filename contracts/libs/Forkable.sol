pragma solidity ^0.4.24;

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

import './ERC223Token.sol';
import './ERC223Receiver_Interface.sol';

/**
 * @title A forkable contract. Balances are modified with a deposit-withdraw scheme.
 */
contract Forkable is ERC223Receiver {
    using SafeMath for uint;

    // Address of the accepted token contract
    ERC223Token public token;
    // Token balances of each user
    mapping (address => uint) public balances;

    event Deposit(address indexed _from, uint _value);
    event Withdraw(address indexed _to, uint _value);

    constructor (address _tokenAddress) public {
        token = ERC223Token(_tokenAddress);
    }

    /**
     * @dev Gets the balance of the specified address.
     * @param _account The address to query the balance of.
     * @return The amount owned by the passed address.
     */
    function balanceOf(address _account) public view returns (uint256) {
        return balances[_account];
    }

    /**
     * @dev Called when tokens are transferred from the token contract to this contract for use.
     *      This is an ERC223-compatible deposit function.
     * @param _from Token sender address.
     * @param _value Amount of tokens.
     */
    function tokenFallback(address _from, uint _value, bytes) public {
        require(msg.sender == address(token));
        balances[_from] = balances[_from].add(_value);

        emit Deposit(_from, _value);
    }

    /**
     * @dev Withdraw all tokens from this contract to the token contract for the sender.
     */
    function withdraw() public {
        uint value = balances[msg.sender];
        delete balances[msg.sender];
        token.transfer(msg.sender, value);

        emit Withdraw(msg.sender, value);
    }
}
