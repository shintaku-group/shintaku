pragma solidity ^0.4.24;

import 'openzeppelin-solidity/contracts/token/ERC20/StandardBurnableToken.sol';

import './ERC223Token.sol';

/**
 * @title Base token contract for oracle.
 */
contract BaseToken is ERC223Token, StandardBurnableToken {
}

