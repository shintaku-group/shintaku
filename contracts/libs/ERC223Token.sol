pragma solidity ^0.4.24;

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';
import 'openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol';

import './ERC223Receiver_Interface.sol';

/**
 * @title Implementation of the ERC223 standard token.
 * @dev See https://github.com/Dexaran/ERC223-token-standard
 */
contract ERC223Token is StandardToken {
    using SafeMath for uint;

    event Transfer(address indexed from, address indexed to, uint value, bytes data);

    modifier enoughBalance(uint _value) {
        require (_value <= balanceOf(msg.sender));
        _;
    }

     /**
     * @dev Transfer the specified amount of tokens to the specified address.
     *      Invokes the `tokenFallback` function if the recipient is a contract.
     *      The token transfer fails if the recipient is a contract
     *      but does not implement the `tokenFallback` function
     *      or the fallback function to receive funds.
     *
     * @param _to Receiver address.
     * @param _value Amount of tokens that will be transferred.
     * @param _data Transaction metadata.
     * @return Success.
     */
    function transfer(address _to, uint _value, bytes _data) public returns (bool success) {
        require(_to != address(0));

        return isContract(_to) ?
            transferToContract(_to, _value, _data) :
            transferToAddress(_to, _value, _data)
        ;
    }

    /**
     * @dev Transfer the specified amount of tokens to the specified address.
     *      This function works the same with the previous one
     *      but doesn't contain `_data` param.
     *      Added due to backwards compatibility reasons.
     *
     * @param _to Receiver address.
     * @param _value Amount of tokens that will be transferred.
     * @return Success.
     */
    function transfer(address _to, uint _value) public returns (bool success) {
        bytes memory empty;

        return transfer(_to, _value, empty);
    }

    /**
     * @dev Assemble the given address bytecode. If bytecode exists then the _addr is a contract.
     * @return If the target is a contract.
     */
    function isContract(address _addr) private view returns (bool is_contract) {
        uint length;

        assembly {
            // Retrieve the size of the code on target address; this needs assembly
            length := extcodesize(_addr)
        }

        return (length > 0);
    }
    
    /**
     * @dev Helper function that transfers to address.
     * @return Success.
     */
    function transferToAddress(address _to, uint _value, bytes _data) private enoughBalance(_value) returns (bool success) {
        balances[msg.sender] = balances[msg.sender].sub(_value);
        balances[_to] = balanceOf(_to).add(_value);

        emit Transfer(msg.sender, _to, _value, _data);

        return true;
    }

    /**
     * @dev Helper function that transfers to contract.
     * @return Success.
     */
    function transferToContract(address _to, uint _value, bytes _data) private enoughBalance(_value) returns (bool success) {
        balances[msg.sender] = balances[msg.sender].sub(_value);
        balances[_to] = balanceOf(_to).add(_value);

        ERC223Receiver receiver = ERC223Receiver(_to);
        receiver.tokenFallback(msg.sender, _value, _data);

        emit Transfer(msg.sender, _to, _value, _data);

        return true;
    }
}

