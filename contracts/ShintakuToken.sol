pragma solidity ^0.4.24;

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

import './libs/BaseToken.sol';

/**
 * @title Shintaku token contract
 * @dev Burnable ERC223 token with set emission curve.
 */
contract ShintakuToken is BaseToken, Ownable {
    using SafeMath for uint;

    string public constant symbol = "SHN";
    string public constant name = "Shintaku";
    uint8 public constant demicals = 18;

    // Unit of tokens
    uint public constant TOKEN_UNIT = (10 ** uint(demicals));

    // Parameters

    // Number of blocks for each period (100000 = ~2-3 weeks)
    uint public PERIOD_BLOCKS;
    // Number of blocks to lock owner balance (50x = ~2 years)
    uint public OWNER_LOCK_BLOCKS;
    // Number of blocks to lock user remaining balances (25x = ~1 year)
    uint public USER_LOCK_BLOCKS;
    // Number of tokens per period during tail emission
    uint public constant TAIL_EMISSION = 400 * (10 ** 3) * TOKEN_UNIT;
    // Number of tokens to emit initially: tail emission is 4% of this
    uint public constant INITIAL_EMISSION_FACTOR = 25;
    // Absolute cap on funds received per period
    // Note: this should be obscenely large to prevent larger ether holders
    //  from monopolizing tokens at low cost. This cap should never be hit in
    //  practice.
    uint public constant MAX_RECEIVED_PER_PERIOD = 10000 ether;

    /**
     * @dev Store relevant data for a period.
     */
    struct Period {
        // Block this period has started at
        uint started;

        // Total funds received this period
        uint totalReceived;
        // Locked owner balance, will unlock after a long time
        uint ownerLockedBalance;
        // Number of tokens to mint this period
        uint minting;

        // Sealed purchases for each account
        mapping (address => bytes32) sealedPurchaseOrders;
        // Balance received from each account
        mapping (address => uint) receivedBalances;
        // Locked balance for each account
        mapping (address => uint) lockedBalances;

        // When withdrawing, withdraw to an alias address (e.g. cold storage)
        mapping (address => address) aliases;
    }

    // Modifiers

    modifier validPeriod(uint _period) {
        require(_period <= currentPeriodIndex());
        _;
    }

    // Contract state

    // List of periods
    Period[] internal periods;

    // Address the owner can withdraw funds to (e.g. cold storage)
    address public ownerAlias;

    // Events

    event NextPeriod(uint indexed _period, uint indexed _block);
    event SealedOrderPlaced(address indexed _from, uint indexed _period, uint _value);
    event SealedOrderRevealed(address indexed _from, uint indexed _period, address indexed _alias, uint _value);
    event OpenOrderPlaced(address indexed _from, uint indexed _period, address indexed _alias, uint _value);
    event Claimed(address indexed _from, uint indexed _period, address indexed _alias, uint _value);

    // Functions

    constructor(address _alias, uint _periodBlocks, uint _ownerLockFactor, uint _userLockFactor) public {
        require(_alias != address(0));
        require(_periodBlocks >= 2);
        require(_ownerLockFactor > 0);
        require(_userLockFactor > 0);

        periods.push(Period(block.number, 0, 0, calculateMinting(0)));
        ownerAlias = _alias;

        PERIOD_BLOCKS = _periodBlocks;
        OWNER_LOCK_BLOCKS = _periodBlocks.mul(_ownerLockFactor);
        USER_LOCK_BLOCKS = _periodBlocks.mul(_userLockFactor);
    }

    /**
     * @dev Go to the next period, if sufficient time has passed.
     */
    function nextPeriod() public {
        uint periodIndex = currentPeriodIndex();
        uint periodIndexNext = periodIndex.add(1);
        require(block.number.sub(periods[periodIndex].started) > PERIOD_BLOCKS);

        periods.push(Period(block.number, 0, 0, calculateMinting(periodIndexNext)));

        emit NextPeriod(periodIndexNext, block.number);
    }

    /**
     * @dev Creates a sealed purchase order.
     * @param _from Account that will purchase tokens.
     * @param _period Period of purchase order.
     * @param _value Purchase funds, in wei.
     * @param _salt Random value to keep purchase secret.
     * @return The sealed purchase order.
     */
    function createPurchaseOrder(address _from, uint _period, uint _value, bytes32 _salt) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(_from, _period, _value, _salt));
    }

    /**
     * @dev Submit a sealed purchase order. Wei sent can be different then sealed value.
     * @param _sealedPurchaseOrder The sealed purchase order.
     */
    function placePurchaseOrder(bytes32 _sealedPurchaseOrder) public payable {
        if (block.number.sub(periods[currentPeriodIndex()].started) > PERIOD_BLOCKS) {
            nextPeriod();
        }
        // Note: current period index may update from above call
        Period storage period = periods[currentPeriodIndex()];
        // Each address can only make a single purchase per period
        require(period.sealedPurchaseOrders[msg.sender] == bytes32(0));

        period.sealedPurchaseOrders[msg.sender] = _sealedPurchaseOrder;
        period.receivedBalances[msg.sender] = msg.value;

        emit SealedOrderPlaced(msg.sender, currentPeriodIndex(), msg.value);
    }

    /**
     * @dev Reveal a sealed purchase order and commit to a purchase.
     * @param _sealedPurchaseOrder The sealed purchase order.
     * @param _period Period of purchase order.
     * @param _value Purchase funds, in wei.
     * @param _period Period for which to reveal purchase order.
     * @param _salt Random value to keep purchase secret.
     * @param _alias Address to withdraw tokens and excess funds to.
     */
    function revealPurchaseOrder(bytes32 _sealedPurchaseOrder, uint _period, uint _value, bytes32 _salt, address _alias) public {
        // Sanity check to make sure user enters an alias
        require(_alias != address(0));
        // Can only reveal sealed orders in the next period
        require(currentPeriodIndex() == _period.add(1));
        Period storage period = periods[_period];
        // Each address can only make a single purchase per period
        require(period.aliases[msg.sender] == address(0));

        // Note: don't *need* to advance period here

        bytes32 h = createPurchaseOrder(msg.sender, _period, _value, _salt);
        require(h == _sealedPurchaseOrder);

        // The value revealed must not be greater than the value previously sent
        require(_value <= period.receivedBalances[msg.sender]);

        period.totalReceived = period.totalReceived.add(_value);
        uint remainder = period.receivedBalances[msg.sender].sub(_value);
        period.receivedBalances[msg.sender] = _value;
        period.aliases[msg.sender] = _alias;

        emit SealedOrderRevealed(msg.sender, _period, _alias, _value);

        // Return any extra balance to the alias
        _alias.transfer(remainder);
    }

    /**
     * @dev Place an unsealed purchase order immediately.
     * @param _alias Address to withdraw tokens to.
     */
    function placeOpenPurchaseOrder(address _alias) public payable {
        // Sanity check to make sure user enters an alias
        require(_alias != address(0));

        if (block.number.sub(periods[currentPeriodIndex()].started) > PERIOD_BLOCKS) {
            nextPeriod();
        }
        // Note: current period index may update from above call
        Period storage period = periods[currentPeriodIndex()];
        // Each address can only make a single purchase per period
        require(period.aliases[msg.sender] == address(0));

        period.totalReceived = period.totalReceived.add(msg.value);
        period.receivedBalances[msg.sender] = msg.value;
        period.aliases[msg.sender] = _alias;

        emit OpenOrderPlaced(msg.sender, currentPeriodIndex(), _alias, msg.value);
    }

    /**
     * @dev Claim previously purchased tokens for an account.
     * @param _from Account to claim tokens for.
     * @param _period Period for which to claim tokens.
     */
    function claim(address _from, uint _period) public {
        // Claiming can only be done at least two periods after submitting sealed purchase order
        require(currentPeriodIndex() > _period.add(1));
        Period storage period = periods[_period];
        require(period.receivedBalances[_from] > 0);

        uint value = period.receivedBalances[_from];
        delete period.receivedBalances[_from];

        (uint emission, uint spent) = calculateEmission(_period, value);
        uint remainder = value.sub(spent);

        address alias = period.aliases[_from];
        // Mint tokens based on spent funds
        mint(alias, emission);

        // Lock up remaining funds for account
        period.lockedBalances[_from] = period.lockedBalances[_from].add(remainder);
        // Lock up spent funds for owner
        period.ownerLockedBalance = period.ownerLockedBalance.add(spent);

        emit Claimed(_from, _period, alias, emission);
    }

    /*
     * @dev Users can withdraw locked balances after the lock time has expired, for an account.
     * @param _from Account to withdraw balance for.
     * @param _period Period to withdraw funds for.
     */
    function withdraw(address _from, uint _period) public {
        require(currentPeriodIndex() > _period);
        Period storage period = periods[_period];
        require(block.number.sub(period.started) > USER_LOCK_BLOCKS);

        uint balance = period.lockedBalances[_from];
        require(balance <= address(this).balance);
        delete period.lockedBalances[_from];

        address alias = period.aliases[_from];
        // Don't delete this, as a user may have unclaimed tokens
        //delete period.aliases[_from];
        alias.transfer(balance);
    }

    /**
     * @dev Contract owner can withdraw unlocked owner funds.
     * @param _period Period to withdraw funds for.
     */
    function withdrawOwner(uint _period) public onlyOwner {
        require(currentPeriodIndex() > _period);
        Period storage period = periods[_period];
        require(block.number.sub(period.started) > OWNER_LOCK_BLOCKS);

        uint balance = period.ownerLockedBalance;
        require(balance <= address(this).balance);
        delete period.ownerLockedBalance;

        ownerAlias.transfer(balance);
    }

    /**
     * @dev The owner can withdraw any unrevealed balances after the deadline.
     * @param _period Period to withdraw funds for.
     * @param _from Account to withdraw unrevealed funds against.
     */
    function withdrawOwnerUnrevealed(uint _period, address _from) public onlyOwner {
        // Must be past the reveal deadline of one period
        require(currentPeriodIndex() > _period.add(1));
        Period storage period = periods[_period];
        require(block.number.sub(period.started) > OWNER_LOCK_BLOCKS);

        uint balance = period.receivedBalances[_from];
        require(balance <= address(this).balance);
        delete period.receivedBalances[_from];

        ownerAlias.transfer(balance);
    }

    /**
     * @dev Calculate the number of tokens to mint during a period.
     * @param _period The period.
     * @return Number of tokens to mint.
     */
    function calculateMinting(uint _period) internal pure returns (uint) {
        // Every period, decrease emission by 5% of initial, until tail emission
        return
            _period < INITIAL_EMISSION_FACTOR ?
            TAIL_EMISSION.mul(INITIAL_EMISSION_FACTOR.sub(_period)) :
            TAIL_EMISSION
        ;
    }

    /**
     * @dev Helper function to get current period index.
     * @return The array index of the current period.
     */
    function currentPeriodIndex() public view returns (uint) {
        assert(periods.length > 0);

        return periods.length.sub(1);
    }

    /**
     * @dev Calculate token emission.
     * @param _period Period for which to calculate emission.
     * @param _value Amount paid. Emissions is proportional to this.
     * @return Number of tokens to emit.
     * @return The spent balance.
     */
    function calculateEmission(uint _period, uint _value) internal view returns (uint, uint) {
        Period storage currentPeriod = periods[_period];
        uint minting = currentPeriod.minting;
        uint totalReceived = currentPeriod.totalReceived;

        uint scaledValue = _value;
        if (totalReceived > MAX_RECEIVED_PER_PERIOD) {
            // If the funds received this period exceed the maximum, scale
            // emission to refund remaining
            scaledValue = _value.mul(MAX_RECEIVED_PER_PERIOD).div(totalReceived);
        }

        uint emission = scaledValue.mul(minting).div(MAX_RECEIVED_PER_PERIOD);
        return (emission, scaledValue);
    }

    /**
     * @dev Mints new tokens.
     * @param _account Account that will receive new tokens.
     * @param _value Number of tokens to mint.
     */
    function mint(address _account, uint _value) internal {
        balances[_account] = balances[_account].add(_value);
        totalSupply_ = totalSupply_.add(_value);
    }

    // Getters

    function getPeriodStarted(uint _period) public view validPeriod(_period) returns (uint) {
        return periods[_period].started;
    }

    function getPeriodTotalReceived(uint _period) public view validPeriod(_period) returns (uint) {
        return periods[_period].totalReceived;
    }

    function getPeriodOwnerLockedBalance(uint _period) public view validPeriod(_period) returns (uint) {
        return periods[_period].ownerLockedBalance;
    }

    function getPeriodMinting(uint _period) public view validPeriod(_period) returns (uint) {
        return periods[_period].minting;
    }

    function getPeriodSealedPurchaseOrderFor(uint _period, address _account) public view validPeriod(_period) returns (bytes32) {
        return periods[_period].sealedPurchaseOrders[_account];
    }

    function getPeriodReceivedBalanceFor(uint _period, address _account) public view validPeriod(_period) returns (uint) {
        return periods[_period].receivedBalances[_account];
    }

    function getPeriodLockedBalanceFor(uint _period, address _account) public view validPeriod(_period) returns (uint) {
        return periods[_period].lockedBalances[_account];
    }

    function getPeriodAliasFor(uint _period, address _account) public view validPeriod(_period) returns (address) {
        return periods[_period].aliases[_account];
    }
}

