pragma solidity ^0.4.24;

import './libs/Forkable.sol';

contract OracleInterface is Forkable {
    // Options for votes
    enum VoteOption { Undecided, True, False, Unknown }

    function isPropositionDecided(address _clique, bytes32 _id) public view returns (bool);
    function getPropositionResult(address _clique, bytes32 _id) public view returns (VoteOption);

    event CliqueNew(address indexed _clique);
    event CliqueWhitelistAdd(address indexed _clique, address indexed _account);
    event CliqueWhitelistRemove(address indexed _clique, address indexed _account);
    event CliqueStage(address indexed _clique, uint _stage, uint indexed _offset);
    event CliqueSubmitProposition(address indexed _clique, address indexed _from, bytes32 indexed _propID, uint _bounty);
    event CliqueRandomNumberReveal(address indexed _clique, uint indexed _offset, address indexed _from, uint _N);
    event CliqueRevealVote(address indexed _clique, uint indexed _offset, address indexed _from, bytes32[] _propIDs, VoteOption[] _choices);
    event CliquePropositionDecided(address indexed _clique, bytes32 indexed _propID, VoteOption indexed _result);
}

