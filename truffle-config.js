/*
 * NB: since truffle-hdwallet-provider 0.0.5 you must wrap HDWallet providers in a 
 * function when declaring them. Failure to do so will cause commands to hang. ex:
 * ```
 * mainnet: {
 *     provider: function() { 
 *       return new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/<infura-key>') 
 *     },
 *     network_id: '1',
 *     gas: 4500000,
 *     gasPrice: 10000000000,
 *   },
 */

require('dotenv').config();

var HDWalletProvider = require("truffle-hdwallet-provider");
var mnemonicRopsten = process.env.MNEMONIC_ROPSTEN;
var mnemonicMainnet = process.env.MNEMONIC_MAINNET;
var accessToken = process.env.INFURA_ACCESS_TOKEN;

module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*", // Match any network id
    },
    /*
    ropsten: {
      provider: function() {
        return new HDWalletProvider(mnemonicRopsten, 'https://ropsten.infura.io/' + accessToken, 0);
      },
      network_id: 3,
      gasPrice: 10000000000,
    },
    mainnet: {
      provider: function() {
        return new HDWalletProvider(mnemonicMainnet, 'https://mainnet.infura.io/' + accessToken, 0);
      },
      network_id: 1,
      gasPrice: 10000000000,
    },
    */
  },
  solc: {
    optimizer: {
      enabled: false,
      runs: 200,
    },
  },
};
