'use strict';

const mine = require('./helpers/mine.js');

const ShintakuToken = artifacts.require('ShintakuToken');

contract('ShintakuToken Emission', function (accounts) {
  let token;

  let owner = accounts[1];

  let PERIOD_BLOCKS;
  let TAIL_EMISSION;
  let INITIAL_EMISSION_FACTOR;

  beforeEach('setting up tests', async function () {
    this.token = await ShintakuToken.new(owner, 2, 42, 42, { from: owner });
    this.PERIOD_BLOCKS = await this.token.PERIOD_BLOCKS();
    this.TAIL_EMISSION = await this.token.TAIL_EMISSION();
    this.INITIAL_EMISSION_FACTOR = await this.token.INITIAL_EMISSION_FACTOR();
  });

  describe('emission schedule', function () {
    it ('should emit tokens based on linear decay with tail emission', async function () {
      for (let i = 0; i < this.INITIAL_EMISSION_FACTOR.plus(2); i++) {
        const period = await this.token.currentPeriodIndex();

        const minting = await this.token.getPeriodMinting(period);
        assert(minting.eq(Math.max(this.TAIL_EMISSION, this.TAIL_EMISSION.times(this.INITIAL_EMISSION_FACTOR.minus(period)))));

        for (let j = 0; j < this.PERIOD_BLOCKS; j++) { mine(); }
        await this.token.nextPeriod();
      }
    });
  });
});
