'use strict';

const assertRevert = require('./helpers/assertRevert.js');
const ether = require('./helpers/ether.js');
const mine = require('./helpers/mine.js');

const BaseTokenMock = artifacts.require('BaseTokenMock');
const Shintaku = artifacts.require('Shintaku');

contract('Shintaku', function (accounts) {
  let token;
  let oracle;

  const stranger = accounts[1];
  const owner = accounts[2];
  const user1 = accounts[3];
  const user2 = accounts[4];

  const baseBalance = 1000;

  beforeEach('setting up tests', async function () {
    this.token = await BaseTokenMock.new({ from: owner });
    this.oracle = await Shintaku.new(this.token.address, { from: owner });

    this.token.debugAddBalance(user1, baseBalance);
    this.token.debugAddBalance(user2, baseBalance);
  });

  describe('token management', function () {
    it('should handle basic deposit and withdraw', async function () {
      {
        const initialBalance = await this.token.balanceOf(user1);
        assert(initialBalance.eq(baseBalance));
      }

      await this.token.transfer(this.oracle.address, baseBalance, { from: user1 });

      {
        const remainingBalance = await this.token.balanceOf(user1);
        assert(remainingBalance.eq(0));
      }
      {
        const lockedBalance = await this.oracle.balanceOf(user1);
        assert(lockedBalance.eq(baseBalance));
      }

      await this.oracle.withdraw({ from: user1 });

      {
        const remainingBalance = await this.oracle.balanceOf(user1);
        assert(remainingBalance.eq(0));
      }
      {
        const tokenBalance = await this.token.balanceOf(user1);
        assert(tokenBalance.eq(baseBalance));
      }
    });
  });

  describe('clique creation', function () {
    it('should create a valid clique', async function () {
      {
        const exists = await this.oracle.doesCliqueExist(user1);
        assert.equal(exists, false);
      }

      await this.oracle.newClique(
        "", // _description
        false, // _permissioned
        false, // _secureRNG
        2, // _minPropositionListSize
        1, // _minBounty
        10, // _maxBounty
        10, // _voteStake
        10, // _stageDuration
        1, // _majorityMul
        2, // _majorityDiv
        1, // _randomFactorOfSafety
      { from: user1 });

      {
        const exists = await this.oracle.doesCliqueExist(user1);
        assert.equal(exists, true);
      }
    });

    describe('should revert', function () {
      it('on more than one clique from same address', async function() {
        await this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          1, // _minBounty
          10, // _maxBounty
          10, // _voteStake
          10, // _stageDuration
          1, // _majorityMul
          2, // _majorityDiv
          1, // _randomFactorOfSafety
        { from: user1 });

        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          1, // _minBounty
          10, // _maxBounty
          10, // _voteStake
          10, // _stageDuration
          1, // _majorityMul
          2, // _majorityDiv
          1, // _randomFactorOfSafety
        { from: user1 }));
      });

      it('on invalid proposition list size', async function () {
        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          1, // _minPropositionListSize (too small)
          1, // _minBounty
          10, // _maxBounty
          10, // _voteStake
          10, // _stageDuration
          1, // _majorityMul
          2, // _majorityDiv
          1, // _randomFactorOfSafety
        { from: user1 }));
      });

      it('on invalid max bounty', async function () {
        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          10, // _minBounty
          1, // _maxBounty (less than min bounty)
          10, // _voteStake
          10, // _stageDuration
          1, // _majorityMul
          2, // _majorityDiv
          1, // _randomFactorOfSafety
        { from: user1 }));
      });

      it('on invalid vote stake', async function () {
        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          1, // _minBounty
          10, // _maxBounty
          0, // _voteStake (zero)
          10, // _stageDuration
          1, // _majorityMul
          2, // _majorityDiv
          1, // _randomFactorOfSafety
        { from: user1 }));
      });

      it('on invalid stage duration', async function () {
        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          1, // _minBounty
          10, // _maxBounty
          10, // _voteStake
          0, // _stageDuration (zero)
          1, // _majorityMul
          2, // _majorityDiv
          1, // _randomFactorOfSafety
        { from: user1 }));
      });

      it('on invalid mul', async function () {
        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          1, // _minBounty
          10, // _maxBounty
          10, // _voteStake
          10, // _stageDuration
          2, // _majorityMul (greater than div)
          1, // _majorityDiv
          1, // _randomFactorOfSafety
        { from: user1 }));
      });

      it('on zero mul', async function () {
        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          1, // _minBounty
          10, // _maxBounty
          10, // _voteStake
          10, // _stageDuration
          0, // _majorityMul (zero)
          2, // _majorityDiv
          1, // _randomFactorOfSafety
        { from: user1 }));
      });

      it('on zero div', async function () {
        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          1, // _minBounty
          10, // _maxBounty
          10, // _voteStake
          10, // _stageDuration
          0, // _majorityMul
          0, // _majorityDiv (zero)
          1, // _randomFactorOfSafety
        { from: user1 }));
      });

      it('on zero random factor of safety', async function () {
        await assertRevert(this.oracle.newClique(
          "", // _description
          false, // _permissioned
          false, // _secureRNG
          2, // _minPropositionListSize
          1, // _minBounty
          10, // _maxBounty
          10, // _voteStake
          10, // _stageDuration
          1, // _majorityMul
          2, // _majorityDiv
          0, // _randomFactorOfSafety (zero)
        { from: user1 }));
      });
    });
  });

  describe('permissioned clique', function () {
    it('should only allow whitelisting from permissioned clique', async function () {
      await this.oracle.newClique(
        "", // _description
        false, // _permissioned
        false, // _secureRNG
        2, // _minPropositionListSize
        1, // _minBounty
        10, // _maxBounty
        10, // _voteStake
        10, // _stageDuration
        1, // _majorityMul
        2, // _majorityDiv
        1, // _randomFactorOfSafety
      { from: user1 });

      await assertRevert(this.oracle.cliqueWhitelistAdd(user1, user2, { from: user1 }));
    });

    it('should only allow whitelisting from clique creator', async function () {
      await this.oracle.newClique(
        "", // _description
        true, // _permissioned
        false, // _secureRNG
        2, // _minPropositionListSize
        1, // _minBounty
        10, // _maxBounty
        10, // _voteStake
        10, // _stageDuration
        1, // _majorityMul
        2, // _majorityDiv
        1, // _randomFactorOfSafety
      { from: user1 });

      await assertRevert(this.oracle.cliqueWhitelistAdd(user1, user2, { from: user2 }));
      await assertRevert(this.oracle.cliqueWhitelistRemove(user1, user2, { from: user2 }));
    });

    it('should add and remove whitelist', async function () {
      await this.oracle.newClique(
        "", // _description
        true, // _permissioned
        false, // _secureRNG
        2, // _minPropositionListSize
        1, // _minBounty
        10, // _maxBounty
        10, // _voteStake
        10, // _stageDuration
        1, // _majorityMul
        2, // _majorityDiv
        1, // _randomFactorOfSafety
      { from: user1 });

      {
        const { logs } = await this.oracle.cliqueWhitelistAdd(user1, user2, { from: user1 });
        assert.equal(logs.length, 1);
        assert.equal(logs[0].event, 'CliqueWhitelistAdd');
        assert.equal(logs[0].args._clique, user1);
        assert.equal(logs[0].args._account, user2);
      }

      {
        const isWhitelisted = await this.oracle.isWhitelistedForClique(user1, user2);
        assert.equal(isWhitelisted, true);
      }

      {
        const isWhitelisted = await this.oracle.isWhitelistedForClique(user1, stranger);
        assert.equal(isWhitelisted, false);
      }

      {
        const { logs } = await this.oracle.cliqueWhitelistRemove(user1, user2, { from: user1 });
        assert.equal(logs.length, 1);
        assert.equal(logs[0].event, 'CliqueWhitelistRemove');
        assert.equal(logs[0].args._clique, user1);
        assert.equal(logs[0].args._account, user2);
      }
    });
  });

  describe('propsitions', function () {
  });

  describe('simulation', function () {
    it('should simulate without error', async function () {
      await this.oracle.newClique(
        "", // _description
        true, // _permissioned
        false, // _secureRNG
        2, // _minPropositionListSize
        1, // _minBounty
        10, // _maxBounty
        10, // _voteStake
        10, // _stageDuration
        1, // _majorityMul
        2, // _majorityDiv
        1, // _randomFactorOfSafety
      { from: user1 });
    });
  });
});
