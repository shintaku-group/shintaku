'use strict';

const assertRevert = require('./helpers/assertRevert.js');
const ether = require('./helpers/ether.js');
const mine = require('./helpers/mine.js');

const ShintakuToken = artifacts.require('ShintakuToken');

contract('ShintakuToken', function (accounts) {
  let token;

  let PERIOD_BLOCKS;
  let OWNER_LOCK_BLOCKS;
  let USER_LOCK_BLOCKS;
  let TAIL_EMISSION;
  let INITIAL_EMISSION_FACTOR;
  let MAX_RECEIVED_PER_PERIOD;

  let stranger = accounts[1];
  let owner = accounts[2];
  let ownerAlias = accounts[3];
  let user1 = accounts[4];
  let user1Alias = accounts[5];
  let user2 = accounts[6];
  let user2Alias = accounts[7];

  beforeEach('setting up tests', async function () {
    this.token = await ShintakuToken.new(ownerAlias, 5, 5, 5, { from: owner });
    this.PERIOD_BLOCKS = await this.token.PERIOD_BLOCKS();
    this.OWNER_LOCK_BLOCKS = await this.token.OWNER_LOCK_BLOCKS();
    this.USER_LOCK_BLOCKS = await this.token.USER_LOCK_BLOCKS();
    this.TAIL_EMISSION = await this.token.TAIL_EMISSION();
    this.INITIAL_EMISSION_FACTOR = await this.token.INITIAL_EMISSION_FACTOR();
    this.MAX_RECEIVED_PER_PERIOD = await this.token.MAX_RECEIVED_PER_PERIOD();
  });

  describe('owner alias', function () {
    it('should set owner alias', async function () {
      const alias = await this.token.ownerAlias();

      assert.equal(alias, ownerAlias);
    });
  });

  describe('total supply', function () {
    it('has zero initial supply', async function () {
      const totalSupply = await this.token.totalSupply();

      assert.equal(totalSupply, 0);
    });
  });

  describe('initial conditions', function () {
    it('should have one period initially', async function () {
      const currentPeriodIndex = await this.token.currentPeriodIndex();

      assert.equal(currentPeriodIndex, 0);
    });

    it('first period should have no balances', async function () {
      const period = await this.token.currentPeriodIndex();

      const totalReceived = await this.token.getPeriodTotalReceived(period);
      assert.equal(totalReceived, 0);

      const ownerLockedBalance = await this.token.getPeriodOwnerLockedBalance(period);
      assert.equal(ownerLockedBalance, 0);
    });
  });

  describe('next period', function () {
    it('should not allow next period too soon, should revert instead', async function () {
      await assertRevert(this.token.nextPeriod());
    });

    it('should allow next period after enough blocks, and emit event', async function () {
      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }

      const { logs } = await this.token.nextPeriod();
      const blockNumber = await web3.eth.blockNumber;

      assert.equal(logs.length, 1);
      assert.equal(logs[0].event, 'NextPeriod');
      assert(logs[0].args._period.eq(1));
      assert(logs[0].args._block.eq(blockNumber));

      const period = await this.token.currentPeriodIndex();
      assert(period.eq(1));
    });
  });

  describe('open payments', function () {
    const value = ether(2);

    it('should accept an open payment directly', async function () {
      const period = await this.token.currentPeriodIndex();
      const { logs } = await this.token.placeOpenPurchaseOrder(user1Alias, { from: user1, value: value });

      assert.equal(logs.length, 1);
      assert.equal(logs[0].event, 'OpenOrderPlaced');
      assert.equal(logs[0].args._from, user1);
      assert(logs[0].args._period.eq(period));
      assert.equal(logs[0].args._alias, user1Alias);
      assert(logs[0].args._value.eq(value));

      const receivedBalance = await this.token.getPeriodReceivedBalanceFor(period, user1);
      assert(receivedBalance.eq(value));

      const totalReceived = await this.token.getPeriodTotalReceived(period);
      assert(totalReceived.eq(value));
    });

    it('should set user alias', async function () {
      const period = await this.token.currentPeriodIndex();
      await this.token.placeOpenPurchaseOrder(user1Alias, { from: user1, value: value });

      const alias = await this.token.getPeriodAliasFor(period, user1);
      assert.equal(alias, user1Alias);
    });

    it('should not allow claiming before two periods have passed', async function () {
      const period = await this.token.currentPeriodIndex();
      await this.token.placeOpenPurchaseOrder(user1Alias, { from: user1, value: value });
      await assertRevert(this.token.claim(period, user1));

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await assertRevert(this.token.claim(period, user1));
    });

    it('should claim tokens for open payment', async function () {
      const period = await this.token.currentPeriodIndex();
      const minting = await this.token.getPeriodMinting(period);
      await this.token.placeOpenPurchaseOrder(user1Alias, { from: user1, value: value });

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();

      // Don't do any remainder calculations here
      const claimed = minting.times(value).div(this.MAX_RECEIVED_PER_PERIOD);

      const { logs } = await this.token.claim(user1, period);
      assert.equal(logs.length, 1);
      assert.equal(logs[0].event, 'Claimed');
      assert.equal(logs[0].args._from, user1);
      assert(logs[0].args._period.eq(period));
      assert.equal(logs[0].args._alias, user1Alias);
      assert(logs[0].args._value.eq(claimed));

      // After claiming, should have 0 balance left
      const receivedBalance = await this.token.getPeriodReceivedBalanceFor(period, user1);
      assert(receivedBalance.eq(0));

      // Pay less than period limit, so this should be 0
      const lockedBalance = await this.token.getPeriodLockedBalanceFor(period, user1);
      assert(lockedBalance.eq(0));

      const balanceOf = await this.token.balanceOf(user1Alias);
      assert(balanceOf.eq(claimed));

      const totalSupply = await this.token.totalSupply();
      assert(totalSupply.eq(claimed));

      // Can't withdraw now, need to wait
      await assertRevert(this.token.withdraw(user1, period));
    });

    it('should claim tokens with a remainder', async function () {
      const period = await this.token.currentPeriodIndex();
      const minting = await this.token.getPeriodMinting(period);
      await this.token.placeOpenPurchaseOrder(user1Alias, { from: user1, value: this.MAX_RECEIVED_PER_PERIOD });
      await this.token.placeOpenPurchaseOrder(user2Alias, { from: user2, value: this.MAX_RECEIVED_PER_PERIOD });

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();

      const claimedPer = minting.div(2);

      {
        const { logs } = await this.token.claim(user1, period);
        assert.equal(logs.length, 1);
        assert.equal(logs[0].event, 'Claimed');
        assert.equal(logs[0].args._from, user1);
        assert(logs[0].args._period.eq(period));
        assert.equal(logs[0].args._alias, user1Alias);
        assert(logs[0].args._value.eq(claimedPer));
      }
      {
        const { logs } = await this.token.claim(user2, period);
        assert.equal(logs.length, 1);
        assert.equal(logs[0].event, 'Claimed');
        assert.equal(logs[0].args._from, user2);
        assert(logs[0].args._period.eq(period));
        assert.equal(logs[0].args._alias, user2Alias);
        assert(logs[0].args._value.eq(claimedPer));
      }

      // After claiming, should have 0 balance left
      const receivedBalance1 = await this.token.getPeriodReceivedBalanceFor(period, user1);
      assert(receivedBalance1.eq(0));
      const receivedBalance2 = await this.token.getPeriodReceivedBalanceFor(period, user2);
      assert(receivedBalance2.eq(0));

      const lockedBalance1 = await this.token.getPeriodLockedBalanceFor(period, user1);
      assert(lockedBalance1.eq(this.MAX_RECEIVED_PER_PERIOD.div(2)));
      const lockedBalance2 = await this.token.getPeriodLockedBalanceFor(period, user2);
      assert(lockedBalance2.eq(this.MAX_RECEIVED_PER_PERIOD.div(2)));

      const balanceOf1 = await this.token.balanceOf(user1Alias);
      assert(balanceOf1.eq(claimedPer));
      const balanceOf2 = await this.token.balanceOf(user2Alias);
      assert(balanceOf2.eq(claimedPer));

      const totalSupply = await this.token.totalSupply();
      assert(totalSupply.eq(claimedPer.times(2)));

      // Can't withdraw now, need to wait
      await assertRevert(this.token.withdraw(user1, period));
      await assertRevert(this.token.withdraw(user2, period));
    });
  });

  describe('sealed payments', function () {
    const value = ether(2);
    const salt = web3.toHex(42);

    it('should accept a sealed payment', async function () {
      const period = await this.token.currentPeriodIndex();
      const sealedPurchaseOrder = await this.token.createPurchaseOrder(user1, period, value, salt);
      const { logs } = await this.token.placePurchaseOrder(sealedPurchaseOrder, { from: user1, value: value.times(2) });

      assert.equal(logs.length, 1);
      assert.equal(logs[0].event, 'SealedOrderPlaced');
      assert.equal(logs[0].args._from, user1);
      assert(logs[0].args._period.eq(period));
      assert(logs[0].args._value.eq(value.times(2)));

      const receivedBalance = await this.token.getPeriodReceivedBalanceFor(period, user1);
      assert(receivedBalance.eq(value.times(2)));
    });

    it('should not allow claiming before two periods have passed', async function () {
      const period = await this.token.currentPeriodIndex();
      const sealedPurchaseOrder = await this.token.createPurchaseOrder(user1, period, value, salt);
      await this.token.placePurchaseOrder(sealedPurchaseOrder, { from: user1, value: value.times(2) });
      await assertRevert(this.token.claim(period, user1));

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      await assertRevert(this.token.claim(period, user1));
    });

    it('should not allow revealing if less than one or more than one periods passed', async function () {
      const period = await this.token.currentPeriodIndex();
      const sealedPurchaseOrder = await this.token.createPurchaseOrder(user1, period, value, salt);
      await this.token.placePurchaseOrder(sealedPurchaseOrder, { from: user1, value: value.times(2) });
      await assertRevert(this.token.revealPurchaseOrder(sealedPurchaseOrder, period, value, salt, user1Alias, { from: user1 }));

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      await assertRevert(this.token.revealPurchaseOrder(sealedPurchaseOrder, period, value, salt, user1Alias, { from: user1 }));
    });

    it('should handle reveal properly', async function () {
      const period = await this.token.currentPeriodIndex();
      const sealedPurchaseOrder = await this.token.createPurchaseOrder(user1, period, value, salt);
      await this.token.placePurchaseOrder(sealedPurchaseOrder, { from: user1, value: value.times(2) });

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      const { logs } = await this.token.revealPurchaseOrder(sealedPurchaseOrder, period, value, salt, user1Alias, { from: user1 });

      assert.equal(logs.length, 1);
      assert.equal(logs[0].event, 'SealedOrderRevealed');
      assert.equal(logs[0].args._from, user1);
      assert(logs[0].args._period.eq(period));
      assert.equal(logs[0].args._alias, user1Alias);
      assert(logs[0].args._value.eq(value));

      const alias = await this.token.getPeriodAliasFor(period, user1);
      assert.equal(alias, user1Alias);

      const receivedBalance = await this.token.getPeriodReceivedBalanceFor(period, user1);
      assert(receivedBalance.eq(value));

      const totalReceived = await this.token.getPeriodTotalReceived(period);
      assert(totalReceived.eq(value));
    });

    it('should only allow one reveal', async function () {
      const period = await this.token.currentPeriodIndex();
      const sealedPurchaseOrder = await this.token.createPurchaseOrder(user1, period, value, salt);
      await this.token.placePurchaseOrder(sealedPurchaseOrder, { from: user1, value: value.times(2) });

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      await this.token.revealPurchaseOrder(sealedPurchaseOrder, period, value, salt, user1Alias, { from: user1 });
      await assertRevert(this.token.revealPurchaseOrder(sealedPurchaseOrder, period, value, salt, user1Alias, { from: user1 }));
    });

    it('should claim tokens for revealed payment', async function () {
      const period = await this.token.currentPeriodIndex();
      const minting = await this.token.getPeriodMinting(period);
      const sealedPurchaseOrder = await this.token.createPurchaseOrder(user1, period, value, salt);
      await this.token.placePurchaseOrder(sealedPurchaseOrder, { from: user1, value: value.times(2) });

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      await this.token.revealPurchaseOrder(sealedPurchaseOrder, period, value, salt, user1Alias, { from: user1 });
      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();

      // Don't do any remainder calculations here
      const claimed = minting.times(value).div(this.MAX_RECEIVED_PER_PERIOD);

      const { logs } = await this.token.claim(user1, period);
      assert.equal(logs.length, 1);
      assert.equal(logs[0].event, 'Claimed');
      assert.equal(logs[0].args._from, user1);
      assert(logs[0].args._period.eq(period));
      assert.equal(logs[0].args._alias, user1Alias);
      assert(logs[0].args._value.eq(claimed));

      // After claiming, should have 0 balance left
      const receivedBalance = await this.token.getPeriodReceivedBalanceFor(period, user1);
      assert(receivedBalance.eq(0));

      // Pay less than period limit, so this should be 0
      const lockedBalance = await this.token.getPeriodLockedBalanceFor(period, user1);
      assert(lockedBalance.eq(0));

      const balanceOf = await this.token.balanceOf(user1Alias);
      assert(balanceOf.eq(claimed));

      const totalSupply = await this.token.totalSupply();
      assert(totalSupply.eq(claimed));

      // Can't withdraw now, need to wait
      await assertRevert(this.token.withdraw(user1, period));
    });

    it('should claim tokens with a remainder', async function () {
      const period = await this.token.currentPeriodIndex();
      const minting = await this.token.getPeriodMinting(period);
      const sealedPurchaseOrder1 = await this.token.createPurchaseOrder(user1, period, this.MAX_RECEIVED_PER_PERIOD, salt);
      await this.token.placePurchaseOrder(sealedPurchaseOrder1, { from: user1, value: this.MAX_RECEIVED_PER_PERIOD });
      const sealedPurchaseOrder2 = await this.token.createPurchaseOrder(user2, period, this.MAX_RECEIVED_PER_PERIOD, salt);
      await this.token.placePurchaseOrder(sealedPurchaseOrder2, { from: user2, value: this.MAX_RECEIVED_PER_PERIOD });

      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();
      await this.token.revealPurchaseOrder(sealedPurchaseOrder1, period, this.MAX_RECEIVED_PER_PERIOD, salt, user1Alias, { from: user1 });
      await this.token.revealPurchaseOrder(sealedPurchaseOrder2, period, this.MAX_RECEIVED_PER_PERIOD, salt, user2Alias, { from: user2 });
      for (let i = 0; i < this.PERIOD_BLOCKS; i++) { mine(); }
      await this.token.nextPeriod();

      const claimedPer = minting.div(2);

      {
        const { logs } = await this.token.claim(user1, period);
        assert.equal(logs.length, 1);
        assert.equal(logs[0].event, 'Claimed');
        assert.equal(logs[0].args._from, user1);
        assert(logs[0].args._period.eq(period));
        assert.equal(logs[0].args._alias, user1Alias);
        assert(logs[0].args._value.eq(claimedPer));
      }
      {
        const { logs } = await this.token.claim(user2, period);
        assert.equal(logs.length, 1);
        assert.equal(logs[0].event, 'Claimed');
        assert.equal(logs[0].args._from, user2);
        assert(logs[0].args._period.eq(period));
        assert.equal(logs[0].args._alias, user2Alias);
        assert(logs[0].args._value.eq(claimedPer));
      }

      // After claiming, should have 0 balance left
      const receivedBalance1 = await this.token.getPeriodReceivedBalanceFor(period, user1);
      assert(receivedBalance1.eq(0));
      const receivedBalance2 = await this.token.getPeriodReceivedBalanceFor(period, user2);
      assert(receivedBalance2.eq(0));

      const lockedBalance1 = await this.token.getPeriodLockedBalanceFor(period, user1);
      assert(lockedBalance1.eq(this.MAX_RECEIVED_PER_PERIOD.div(2)));
      const lockedBalance2 = await this.token.getPeriodLockedBalanceFor(period, user2);
      assert(lockedBalance2.eq(this.MAX_RECEIVED_PER_PERIOD.div(2)));

      const balanceOf1 = await this.token.balanceOf(user1Alias);
      assert(balanceOf1.eq(claimedPer));
      const balanceOf2 = await this.token.balanceOf(user2Alias);
      assert(balanceOf2.eq(claimedPer));

      const totalSupply = await this.token.totalSupply();
      assert(totalSupply.eq(claimedPer.times(2)));

      // Can't withdraw now, need to wait
      await assertRevert(this.token.withdraw(user1, period));
      await assertRevert(this.token.withdraw(user2, period));
    });
  });

  describe('withdrawals', function () {
    describe('user', function () {
      it('should only allow withdrawals after lock blocks', async function () {
        const period = await this.token.currentPeriodIndex();
        await this.token.placeOpenPurchaseOrder(user1Alias, { from: user1, value: this.MAX_RECEIVED_PER_PERIOD.times(3) });

        for (let b = 0; b < this.USER_LOCK_BLOCKS.div(2);) {
          await assertRevert(this.token.withdraw(user1, period));
          for (let i = 0; i < this.PERIOD_BLOCKS; i++, b++) { mine(); }
          await this.token.nextPeriod();
        }

        await this.token.claim(user1, period);
        {
          const lockedBalance = await this.token.getPeriodLockedBalanceFor(period, user1);
          assert(lockedBalance.eq(this.MAX_RECEIVED_PER_PERIOD.times(2)));
        }

        for (let b = 0; b < this.USER_LOCK_BLOCKS.div(2);) {
          for (let i = 0; i < this.PERIOD_BLOCKS; i++, b++) { mine(); }
          await this.token.nextPeriod();
        }

        await this.token.withdraw(user1, period);
        {
          const lockedBalance = await this.token.getPeriodLockedBalanceFor(period, user1);
          assert(lockedBalance.eq(0));
        }
      });
    });

    describe('owner', function () {
      it('should only allow withdrawals after lock blocks', async function () {
        const period = await this.token.currentPeriodIndex();
        await this.token.placeOpenPurchaseOrder(user1Alias, { from: user1, value: this.MAX_RECEIVED_PER_PERIOD.times(3) });

        for (let b = 0; b < this.OWNER_LOCK_BLOCKS.div(2);) {
          await assertRevert(this.token.withdrawOwner(period, { from: owner }));
          for (let i = 0; i < this.PERIOD_BLOCKS; i++, b++) { mine(); }
          await this.token.nextPeriod();
        }

        await this.token.claim(user1, period);
        {
          const ownerLockedBalance = await this.token.getPeriodOwnerLockedBalance(period);
          assert(ownerLockedBalance.eq(this.MAX_RECEIVED_PER_PERIOD));
        }

        for (let b = 0; b < this.OWNER_LOCK_BLOCKS.div(2);) {
          for (let i = 0; i < this.PERIOD_BLOCKS; i++, b++) { mine(); }
          await this.token.nextPeriod();
        }

        await this.token.withdrawOwner(period, { from: owner });
        {
          const ownerLockedBalance = await this.token.getPeriodOwnerLockedBalance(period);
          assert(ownerLockedBalance.eq(0));
        }
      });
    });
  });
});
