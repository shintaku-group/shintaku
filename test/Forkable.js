'use strict';

const assertRevert = require('./helpers/assertRevert.js');
const ether = require('./helpers/ether.js');
const mine = require('./helpers/mine.js');

const BaseTokenMock = artifacts.require('BaseTokenMock');
const Forkable = artifacts.require('Forkable');

contract('Forkable', function (accounts) {
  let token;
  let forkable;

  let stranger = accounts[1];
  let owner = accounts[2];
  let user1 = accounts[3];
  let user2 = accounts[4];

  beforeEach('setting up tests', async function () {
    this.token = await BaseTokenMock.new({ from: owner });
    this.forkable = await Forkable.new(this.token.address, { from: owner });
  });

  describe('initial comditions', function () {
    it('should have zero balance', async function () {
      const balanceOf = await this.forkable.balanceOf(user1);
      assert(balanceOf.eq(0));
    });
  });

  describe('deposits', function () {
    const value = 100;

    it('should accept deposits from valid token', async function () {
      await this.token.debugAddBalance(user1, value);
      const { logs } = await this.token.transfer(this.forkable.address, value, { from: user1 });

      // TODO why can't we get inner events?
      assert.equal(logs.length, 1);
      assert.equal(logs[0].logIndex, 1);
      assert(logs[0].args.value.eq(value));

      const balanceOf = await this.forkable.balanceOf(user1);
      assert(balanceOf.eq(value));

      const tokenBalanceOf = await this.token.balanceOf(user1);
      assert(tokenBalanceOf.eq(0));
    });
  });

  describe('withdrawals', function () {
    const value = 100;

    it('should withdraw all tokens', async function () {
      await this.token.debugAddBalance(user1, value);
      await this.token.transfer(this.forkable.address, value, { from: user1 });

      const { logs } =  await this.forkable.withdraw({ from: user1 });
      assert.equal(logs.length, 1);
      assert.equal(logs[0].event, "Withdraw");
      assert.equal(logs[0].args._to, user1);
      assert(logs[0].args._value.eq(value));

      const balanceOf = await this.forkable.balanceOf(user1);
      assert(balanceOf.eq(0));

      const tokenBalanceOf = await this.token.balanceOf(user1);
      assert(tokenBalanceOf.eq(value));
    });
  });
});
