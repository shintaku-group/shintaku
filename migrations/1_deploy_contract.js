var ShintakuToken = artifacts.require("./ShintakuToken.sol");
var Shintaku = artifacts.require("./Shintaku.sol");

module.exports = function(deployer, network, accounts) {
  deployer.deploy(ShintakuToken, accounts[0], 100000, 50, 25, { overwrite: false }).then(function() {
      return deployer.deploy([
          [Shintaku, ShintakuToken.address, { overwrite: false }],
      ]);
  });
};
